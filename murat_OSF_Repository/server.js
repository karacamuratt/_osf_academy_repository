﻿//PACKAGES DEFİNE
var express = require('express');
var fs = require('fs');
var Multer = require('multer');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser')
var path = require('path');

//EJS Layouts
var ejsLayouts = require('express-ejs-layouts');

// Use Express Module
var app = express();

app.use(cookieParser());

//Use to Ejs-Layouts
app.use(ejsLayouts);


//Folder that everyone can see (Herkesin görebileceği klasörü belirleme)
app.use('/public', express.static(path.join(__dirname, 'public')));

//Session
app.use(session({
    secret: 'gizlianahtar',
    resave: false,
    saveUninitialized: true
}));

// Define View Engine (EJS Modülünü programa tanıtma)
app.set('view engine', 'ejs');
// Show to Path the View Engine
app.set('views', path.join(__dirname, './views'));

//Editing Request Object (Parsing) (Kullanıcıdan verileri düzenli olarak alabilmemizi sağlar)
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//adding routers
require('./route/routeManager.js')(app);

//Listen to port number | 3000
app.listen(3000);
console.log("Listening to 3000 port!");