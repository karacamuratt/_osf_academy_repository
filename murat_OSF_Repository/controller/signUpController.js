var path = require('path');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";



module.exports.SignUp = function(req,res){

        if(req.session.User == null)
        {
             res.render('SignUp-Shop/signup',{Username:null,Result:"1",varb:"0"});
        }
        else
        {
             res.render('SignUp-Shop/signup',{Username:req.session.User});            
        }

}

module.exports.SignUpPost = function(req,res){
    
        if(req.session.User == null)
        {
            var username1 = req.body.username;
            var email1 = req.body.email;
            var password1 = req.body.password;


            MongoClient.connect(url, function(err, db) {
                
                if (err) 
                {
                    throw err;
                }

                // USERS
                db.collection("Users").findOne(
                    {'$or': [{'username': username1},{'email':email1}]},
                    function(err,result){
                  
                if (err) 
                {
                   throw err;
                }
                else{
                        if(result == null)
                        {
                            //SAVE NEW ACCOUNT

                            db.collection("Users").insertOne({
                                    username:username1,
                                    email:email1,
                                    password:password1,
                                    shopping:[{}]
                            },function(err,result){

                                if(err){
                                    throw err;
                                }
                                else{
                                    res.redirect('/login');
                                }

                            });

                                 
                        }
                        else{


                                if(result.username == username1)
                                {
                                    console.log("Ayni isimde kayitli kullanici var...");
                                    res.render("SignUp-Shop/signup",{Username:null,Result:"User",varb:username1}); 
                                }
                                else
                                {
                                    console.log("Ayni eposta kayitli kullanici var...");
                                    res.render("SignUp-Shop/signup",{Username:null,Result:"Email",varb:email1}); 
                                }
                                  
                        }                
                }
                    
                db.close();

                });
            });
        
        }
        else
        {
            res.render('SignUp-Shop/signup',{Username:req.session.User});            
        }
    
}