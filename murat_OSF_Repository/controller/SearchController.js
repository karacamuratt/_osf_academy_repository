var path = require('path');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";


module.exports.PostSearch = function (req, res) {
    
        MongoClient.connect(url, function(err, db) 
        {
                var product = req.body.autocomplete;
                if (err) 
                {
                        throw err;
                }
        
                        db.collection("products").findOne({name:product},function(err,result){
                                
                                if (err) 
                                {
                                        throw err;
                                }
                                else
                                {
                                        if(result != null)
                                        {
                                                if(req.session.User !=null)
                                                {
                                                        res.render('Search/search',{Result:result,Username:req.session.User});
                                                }
                                                else
                                                {
                                                        res.render('Search/search',{Result:result,Username:null});   
                                                }    
                                        }
                                        else
                                        {           
                                                if(req.session.User !=null)
                                                {
                                                        res.render('Search/searchNotFound',{Username:req.session.User});
                                                }
                                                else
                                                {
                                                        res.render('Search/searchNotFound',{Username:null});   
                                                }      
                                        }                
                                }
                            
                                db.close();
                        });
        });
}