var path = require('path');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";

// Login Page with EJS
module.exports.login = function (req, res) {
    

        if(req.session.User == null)
        {
             res.render('Login-Shop/login-shop',{Username:null, Result:"1"});
        }
        else
        {
             res.render('Login-Shop/login-shop',{Username:req.session.User});            
        }
}

module.exports.loginPost = function(req,res){

        var username = req.body.username;
        var password = req.body.password;

        MongoClient.connect(url, function(err, db) {
                if (err) 
                {
                    throw err;
                }

                // USERS
                db.collection("Users").findOne(
                        {'$and': [{'username': username},{'password':password}]},
                        function(err,result){
                  
                if (err) 
                {
                   throw err;
                }
                else{
                        if(result != null)
                        {
                                //Start Session
                                req.session.User = result;
                                console.log(req.session.User.username+" giris yapti...");
                                res.redirect('/');
                        }
                        else{
                                console.log("Kayitli kullanici bulunamadi...");

                                res.render("Login-Shop/login-shop",{Username:null,Result:"0"});    
                        }                
                }
                    
                db.close();

                });
        });

}
