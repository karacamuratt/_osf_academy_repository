var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";
var path = require('path');

// Index Page with EJS '/'
module.exports.index = function (req, res) { 
    
    //res.sendFile(path.join(__dirname, "../views/index.html"));

    MongoClient.connect(url, function(err, db) {
    if (err) 
    {
        throw err;
    }

    console.log("Connected to YourShop Database");

    // INDEX ALL DATA QUERY
    db.collection("categories").find().toArray(function(err, result) {
      if (err) {
          throw err;
        }

        if(req.session.User != null)
        {
            res.render('home/index',{Result:result, Username:req.session.User});
        }
        else
        {
            res.render('home/index',{Result:result, Username:null});
        } 
      
      db.close();
    });

    });
}