var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";
var path = require('path');

module.exports.AutoComplete = function (req, res) { 

    MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
            db.collection("products").find().toArray(function(err, result) {
            
                if (err) 
                {
                    throw err;
                }
                else
                {
                    res.json(JSON.stringify(result));
                }
            
                db.close();
            });
    });
}