var fs = require('fs');
var path=require('path');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";


module.exports.WomensPage = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").find({id:"womens"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Womens/womens',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Womens/womens',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });
    
        
}

module.exports.WomensClothing = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").findOne({id:"womens"},function(err,result){
          if (err) {
              throw err;
            }
    
            if(req.session.User != null)
                {
                    res.render('Womens/clothing',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Womens/clothing',{Result:result, Username:null});
                } 

            db.close();
        });
    });

}

module.exports.WomensJewelry = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").findOne({id:"womens"},function(err,result){
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Womens/jewelry',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Womens/jewelry',{Result:result, Username:null});
                } 

        db.close();
        });
    });
    
}

module.exports.WomensAccessories = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").findOne({id:"womens"},function(err,result){
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Womens/accessories',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Womens/accessories',{Result:result, Username:null});
                } 

        db.close();
        });
    });
        
        
}
    
// -------------------------------

module.exports.ClothingOutFits = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"womens-clothing-outfits"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Womens-Clothing/outfits',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Womens-Clothing/outfits',{Result:result, Username:null});
                } 

          
          db.close();
        });
    });

}

module.exports.ClothingTops = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-clothing-tops"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Clothing/tops',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Clothing/tops',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.ClothingDresses = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-clothing-dresses"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Clothing/dresses',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Clothing/dresses',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.ClothingBottoms = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-clothing-bottoms"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Clothing/bottoms',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Clothing/bottoms',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.ClothingJackets = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-clothing-jackets-coats"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Clothing/jackets',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Clothing/jackets',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.ClothingFeelingReds = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-clothing-feeling-reds"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Clothing/feelingreds',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Clothing/feelingreds',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

// -------------------------------------------------

module.exports.WomensJewelryEarrings = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-jewelry-earrings"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Jewelry/earrings',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Jewelry/earrings',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.WomensJewelryBracelets = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-jewelry-bracelets"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Jewelry/bracelets',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Jewelry/bracelets',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.WomensJewelryNecklaces = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-jewelry-necklaces"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Jewelry/necklaces',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Jewelry/necklaces',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

// ---------------------------------------

module.exports.AccessoriesScarves = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-accessories-scarves"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Accessories/scarves',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Accessories/scarves',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

module.exports.AccessoriesShoes = function(req,res){
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("products").find({primary_category_id:"womens-accessories-shoes"}).toArray(function(err, result) {
              if (err) {
                  throw err;
                }
        
                    if(req.session.User != null)
                    {
                        res.render('Womens-Accessories/shoes',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('Womens-Accessories/shoes',{Result:result, Username:null});
                    } 
              
              db.close();
            });
        });
    
}

//-----------------------------------------

module.exports.WomensMoreInfo = function(req,res){
    
        //Product Id (Unique)
        var Product_id = req.params.id;
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
    
            // INDEX ALL DATA QUERY
            db.collection("products").findOne({id:Product_id},function(err,result){
                
                if (err){
                    throw err;
                }
                
                    if(req.session.User != null)
                    {
                        res.render('MoreInfo/moreinfo',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('MoreInfo/moreinfo',{Result:result, Username:null});
                    } 

            db.close();
            });
    
        });
}

module.exports.WomensMoreInfoOne = function(req,res){
    
        //Product Id (Unique)
        var Product_id = req.params.id;
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
    
            // INDEX ALL DATA QUERY
            db.collection("products").findOne({id:Product_id},function(err,result){
                
                if (err){
                    throw err;
                }

                    if(req.session.User != null)
                    {
                        res.render('MoreInfo/moreinfoOne',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('MoreInfo/moreinfoOne',{Result:result, Username:null});
                    } 
                  
            db.close();
            
            });
    
        });
}