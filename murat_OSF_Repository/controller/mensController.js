var fs = require('fs');
var path=require('path');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";


module.exports.MensPage = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").find({id:"mens"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens/mens',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens/mens',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

}

module.exports.MensClothing = function(req,res){
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").findOne({id:"mens"},function(err,result){
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens/clothing',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens/clothing',{Result:result, Username:null});
                } 
            
        db.close();
        });
    });

}

module.exports.MensAccessories = function(req,res){
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("categories").findOne({id:"mens"},function(err,result){
          if (err) {
              throw err;
            }
    
            if(req.session.User != null)
                {
                    res.render('Mens/accessories',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens/accessories',{Result:result, Username:null});
                } 

        db.close();
        });
    });

}

// ------------------------

module.exports.MensSuit = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-clothing-suits"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Clothing/Mens-Suit/Mens-Suit',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Clothing/Mens-Suit/Mens-Suit',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

    
}

module.exports.MensJacket = function(req,res){
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-clothing-jackets"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Clothing/Mens-Jacket/Mens-Jacket',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Clothing/Mens-Jacket/Mens-Jacket',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

    
}

module.exports.MensDress = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-clothing-dress-shirts"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Clothing/Mens-Dress/Mens-Dress',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Clothing/Mens-Dress/Mens-Dress',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

    
}

module.exports.MensShorts = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-clothing-shorts"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Clothing/Mens-Shorts/Mens-Shorts',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Clothing/Mens-Shorts/Mens-Shorts',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

    
}

module.exports.MensPants = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-clothing-pants"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Clothing/Mens-Pants/Mens-Pants',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Clothing/Mens-Pants/Mens-Pants',{Result:result, Username:null});
                } 
          
          db.close();
        });
    });

    
}


//-----------------------------------

module.exports.MensAccessoriesTies = function(req,res){

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-accessories-ties"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Accessories/Mens-Accessories-Ties/Mens-Accessories-Ties',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Accessories/Mens-Accessories-Ties/Mens-Accessories-Ties',{Result:result, Username:null});
                }
          
          db.close();
        });
    });
 

}

module.exports.MensAccessoriesGloves = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-accessories-gloves"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
            if(req.session.User != null)
                {
                    res.render('Mens-Accessories/Mens-Accessories-Gloves/Mens-Accessories-Gloves',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Accessories/Mens-Accessories-Gloves/Mens-Accessories-Gloves',{Result:result, Username:null});
                }
          
          db.close();
        });
    });

}

module.exports.MensAccessoriesLuggage = function(req,res){
    
    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }
    
        // INDEX ALL DATA QUERY
        db.collection("products").find({primary_category_id:"mens-accessories-luggage"}).toArray(function(err, result) {
          if (err) {
              throw err;
            }
    
                if(req.session.User != null)
                {
                    res.render('Mens-Accessories/Mens-Accessories-Luggage/Mens-Accessories-Luggage',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('Mens-Accessories/Mens-Accessories-Luggage/Mens-Accessories-Luggage',{Result:result, Username:null});
                }
          
          db.close();
        });
    });

}

//------------------------------------

module.exports.MensMoreInfo = function(req,res){

    //Product Id (Unique)
    var Product_id = req.params.id;

    MongoClient.connect(url, function(err, db) {
        if (err) 
        {
            throw err;
        }

        // INDEX ALL DATA QUERY
        db.collection("products").findOne({id:Product_id},function(err,result){
            
            if (err){
                throw err;
            }


                if(req.session.User != null)
                {
                    res.render('MoreInfo/moreinfo',{Result:result, Username:req.session.User});
                }
                else
                {
                    res.render('MoreInfo/moreinfo',{Result:result, Username:null});
                }

                db.close();
        });

    });
}

module.exports.MensMoreInfoOne = function(req,res){
    
        //Product Id (Unique)
        var Product_id = req.params.id;
    
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
    
            // INDEX ALL DATA QUERY
            db.collection("products").findOne({id:Product_id},function(err,result){
                
                if (err){
                    throw err;
                }
                
                    if(req.session.User != null)
                    {
                        res.render('MoreInfo/moreinfoOne',{Result:result, Username:req.session.User});
                    }
                    else
                    {
                        res.render('MoreInfo/moreinfoOne',{Result:result, Username:null});
                    }

                    db.close();
            });
    
        });
}
