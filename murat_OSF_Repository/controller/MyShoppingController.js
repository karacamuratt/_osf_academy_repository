var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";
var path = require('path');


module.exports.MyShopping = function (req, res) { 

    var user = req.session.User.username;
    
    if(req.session.User != null)
    {
        //My Products
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // INDEX ALL DATA QUERY
            db.collection("Users").find({username:user.toString()}).toArray(function(err, result) {
                
                if (err) 
                {
                  throw err;
                }else{

                    var total_euro = 0;
                    var total_dolar = 0;
                    var total_tl = 0;
                    
                    for(var i=1;i<result[0].shopping.length;i++)
                    {
                        if(result[0].shopping[i][3].currency =="usd")
                        {
                            total_dolar += parseFloat(result[0].shopping[i][2].price)
                        }
                        else if(result[0].shopping[i][3].currency =="euro")
                        {
                            total_euro += parseFloat(result[0].shopping[i][2].price)
                        }
                        else
                        {
                            total_tl += parseFloat(result[0].shopping[i][2].price) 
                        }
                        
                    }

                    if(result[0].shopping.length > 1)
                    {
                        res.render('MyShopping/MyShop',{Result:result,TotalEuro:total_euro,TotalDolar:total_dolar,TotalTl:total_tl,Username:req.session.User});
                    }
                    else
                    {
                        res.render('MyShopping/MyShopNotFound',{Result:"",Username:req.session.User});
                    }

                }
                
                

            });

            db.close();
        });


    }
    else
    {
        res.redirect('/');
    }

}

module.exports.RemoveProduct = function(req,res){

    var user = req.session.User.username;
    var id = req.params.id;

    if(req.session.User != null)
    {
        //My Products
        MongoClient.connect(url, function(err, db) {
            if (err) 
            {
                throw err;
            }
        
            // REMOVE
            db.collection("Users").update({username:user.toString()},{  $pull: { shopping: { $elemMatch: { productID:id } } } },{ multi: true  },function(err, result) {
                
                if (err) 
                {
                  throw err;
                }   
                else
                {
                    res.redirect('/myshopping');
                }         
            });

            db.close();
        });
    }

}

