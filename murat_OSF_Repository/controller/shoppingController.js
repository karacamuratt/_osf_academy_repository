var fs = require('fs');
var path=require('path');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/YourShop";


module.exports.ShoppingAdd = function(req,res){

        if(req.session.User != null)
        {
            
            var current = req.params.id;
            var user = req.session.User.username;

            // Select price and currency
            var currency2 = req.params.currency;
            var price2 = req.params.price;

            //Variables
            var id;
            var name;
            var price;
            var image;

            //Current Product Query
            MongoClient.connect(url, function(err, db) {
                if (err) 
                {
                    throw err;
                }

                db.collection("products").find({id:current.toString()}).toArray(function(err,result){

                    if(err)
                    {
                        throw err;
                    }
                    else
                    {
                        id = current.toString();
                        name = result[0].name.toString();
                        price = result[0].price.toString();
                        image = result[0].image_groups[0].images[0].link.toString();
                    }
                });

                db.close();

            });

            // Current User add Product 
            MongoClient.connect(url, function(err, db) {
                if (err) 
                {
                    throw err;
                }

                if(id !=null || name !=null || price !=null || image !=null)
                {

                    db.collection("Users").update({username:user.toString()},{$push: {shopping:[{productID:id},{name:name},{price:price2},{currency:currency2},{image:image}]}},function(err,result){
                        
                        if(err)
                        {
                            throw err;
                        }
                        
                    });

                    res.redirect('/myshopping');
                }
                else
                {
                    res.redirect('/');
                }

                db.close();

                
            
            });

        }
        else
        {
            res.redirect('/');
        } 

}