var HomeRoute = require('./homeRoute');
var MensRoute = require('./MensRoute');
var WomensRoute = require('./WomensRoute');
var LoginRoute = require('./LoginRoute');
var LogOutRoute = require('./LogOutRoute');
var SignUpRoute = require('./SignUpRoute');
var ShoppingRoute = require('./ShoppingRoute');
var MyShoppingRoute = require('./MyShoppingRoute');
var SearchRoute = require('./SearchRoute');
var autocompleteRoute = require('./autocompleteRoute');


module.exports = function(app){

    app.use('/',HomeRoute);
    app.use('/mens',MensRoute);
    app.use('/womens',WomensRoute);
    app.use('/login',LoginRoute);
    app.use('/logout',LogOutRoute);
    app.use('/signup',SignUpRoute);
    app.use('/shopping',ShoppingRoute);
    app.use('/myshopping',MyShoppingRoute);
    app.use('/search',SearchRoute);
    app.use('/autocomplete',autocompleteRoute);

}
