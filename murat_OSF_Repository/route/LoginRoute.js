var express = require('express');

// Express --> Router Module
var router = express.Router();

var ControllerLogin = require('../controller/loginController.js');

router.get('/',ControllerLogin.login);
router.post('/',ControllerLogin.loginPost);

module.exports = router;
