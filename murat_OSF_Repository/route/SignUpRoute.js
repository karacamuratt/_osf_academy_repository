var express = require('express');

// Express --> Router Module
var router = express.Router();

var ControllerSignUp = require('../controller/signUpController.js');

router.get('/',ControllerSignUp.SignUp);
router.post('/',ControllerSignUp.SignUpPost);

module.exports = router;
