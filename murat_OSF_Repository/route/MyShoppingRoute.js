var express = require('express');

// Express --> Router Module
var router = express.Router();

var MyShoppingController = require('../controller/MyShoppingController.js');

router.get('/',MyShoppingController.MyShopping);
router.get('/:id',MyShoppingController.RemoveProduct);

module.exports = router;