var express = require('express');

// Express --> Router Module
var router = express.Router();

var shoppingController = require('../controller/shoppingController.js');

router.get('/:id&:price&:currency',shoppingController.ShoppingAdd);

module.exports = router;