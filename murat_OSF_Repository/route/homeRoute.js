var express = require('express');

// Express --> Router Module
var router = express.Router();

var ControllerHome = require('../controller/homeController.js');

router.get('/',ControllerHome.index);

module.exports = router;