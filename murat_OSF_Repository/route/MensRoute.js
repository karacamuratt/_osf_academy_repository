var express = require('express');

// Express --> Router Module
var router = express.Router();

var mensController = require('../controller/mensController.js');

router.get('/',mensController.MensPage);
router.get('/clothing',mensController.MensClothing);
router.get('/accessories',mensController.MensAccessories);

router.get('/clothing/suit',mensController.MensSuit);
router.get('/clothing/jacket',mensController.MensJacket);
router.get('/clothing/dress',mensController.MensDress);
router.get('/clothing/shorts',mensController.MensShorts);
router.get('/clothing/pants',mensController.MensPants);


router.get('/accessories/ties',mensController.MensAccessoriesTies);
router.get('/accessories/gloves',mensController.MensAccessoriesGloves);
router.get('/accessories/luggage',mensController.MensAccessoriesLuggage);

//More Info
router.get('/clothing/suit/:id',mensController.MensMoreInfo);
router.get('/clothing/jacket/:id',mensController.MensMoreInfoOne);
router.get('/clothing/dress/:id',mensController.MensMoreInfoOne);
router.get('/clothing/shorts/:id',mensController.MensMoreInfo);
router.get('/clothing/pants/:id',mensController.MensMoreInfo);


router.get('/accessories/ties/:id',mensController.MensMoreInfo);
router.get('/accessories/gloves/:id',mensController.MensMoreInfoOne);
router.get('/accessories/luggage/:id',mensController.MensMoreInfoOne);




module.exports = router;