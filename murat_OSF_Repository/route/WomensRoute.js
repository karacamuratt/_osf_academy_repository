var express = require('express');

// Express --> Router Module
var router = express.Router();

var womensController = require('../controller/womensController');

router.get('/',womensController.WomensPage);

router.get('/clothing',womensController.WomensClothing);
router.get('/jewelry',womensController.WomensJewelry);
router.get('/accessories',womensController.WomensAccessories);


router.get('/clothing/outfits',womensController.ClothingOutFits);
router.get('/clothing/tops',womensController.ClothingTops);
router.get('/clothing/dresses',womensController.ClothingDresses);
router.get('/clothing/bottoms',womensController.ClothingBottoms);
router.get('/clothing/jackets',womensController.ClothingJackets);
router.get('/clothing/feelingreds',womensController.ClothingFeelingReds);

router.get('/jewelry/earrings',womensController.WomensJewelryEarrings);
router.get('/jewelry/bracelets',womensController.WomensJewelryBracelets);
router.get('/jewelry/necklaces',womensController.WomensJewelryNecklaces);

router.get('/accessories/scarves',womensController.AccessoriesScarves);
router.get('/accessories/shoes',womensController.AccessoriesShoes);

// More Info
router.get('/clothing/tops/:id',womensController.WomensMoreInfo);
router.get('/clothing/dresses/:id',womensController.WomensMoreInfo);
router.get('/clothing/bottoms/:id',womensController.WomensMoreInfo);

router.get('/jewelry/earrings/:id',womensController.WomensMoreInfoOne);
router.get('/jewelry/necklaces/:id',womensController.WomensMoreInfoOne);

router.get('/accessories/scarves/:id',womensController.WomensMoreInfoOne);
router.get('/accessories/shoes/:id',womensController.WomensMoreInfoOne);

module.exports = router;