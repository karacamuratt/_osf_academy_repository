var express = require('express');

// Express --> Router Module
var router = express.Router();

var SearchController = require('../controller/SearchController.js');

router.post('/',SearchController.PostSearch);

module.exports = router;