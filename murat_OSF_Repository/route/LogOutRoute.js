var express = require('express');

// Express --> Router Module
var router = express.Router();

var LogOutController = require('../controller/logOutController.js');

router.get('/',LogOutController.LogOutSession);


module.exports = router;