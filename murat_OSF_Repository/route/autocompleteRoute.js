var express = require('express');

// Express --> Router Module
var router = express.Router();

var autocompleteController = require('../controller/autocompleteController.js');

router.get('/',autocompleteController.AutoComplete);
router.post('/',autocompleteController.AutoComplete);

module.exports = router;